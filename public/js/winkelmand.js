let objShoppingCart = JSON.parse(localStorage.getItem('ShoppingCart'))
/*
let data = [{id:1,name:'Spanje',price:5},{id:2,name:'Nederland',price:10},{id:3,name:'Duitsland',price:8}];
*/

window.onload = function () {
const ul = document.querySelector('.winkelmand-body');
    console.log(objShoppingCart);
objShoppingCart.forEach(item => {
    let li = document.createElement('li');
    ul.appendChild(li);

    let div = document.createElement('div');
    div.classList.add("item");
    li.appendChild(div);

    let selectDelete = document.createElement('div');
    selectDelete.classList.add("verwijder-selecteer");
    div.appendChild(selectDelete);
    let deleteButton = document.createElement('a');
    deleteButton.classList.add("myButton");
    deleteButton.innerText = "x";
    selectDelete.appendChild(deleteButton);

    let imageDiv = document.createElement('div');
    imageDiv.classList.add("image");
    div.appendChild(imageDiv);
    let image = document.createElement('img');
    imageDiv.appendChild(image);

    let description = document.createElement('div');
    description.classList.add("description");
    div.appendChild(description);
    let pDescription = document.createElement('p');
    description.innerText = item.name;
    description.appendChild(pDescription);

    let hoeveelheid = document.createElement('div');
    description.classList.add("hoeveelheid");
    div.appendChild(hoeveelheid);
    let minus = document.createElement('a');
    minus.classList.add("aantalChange");
    minus.innerText = "-";
    let inputHoeveelheid = document.createElement('input');
    inputHoeveelheid.classList.add("item-aantal");
    let plus = document.createElement('a');
    plus.classList.add("aantalChange");
    plus.innerText = "+";
    hoeveelheid.appendChild(minus)
    hoeveelheid.appendChild(inputHoeveelheid);
    hoeveelheid.appendChild(plus);

    let totalPrice = document.createElement('div');
    totalPrice.classList.add("total-price");
    div.appendChild(totalPrice);
    let pTotalPrice = document.createElement('p');
    pTotalPrice.classList.add("item-total");
    pTotalPrice.innerText = item.price;
    totalPrice.appendChild(pTotalPrice);
})
};