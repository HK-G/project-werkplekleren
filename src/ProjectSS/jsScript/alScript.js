window.addEventListener("load", CreateStorageElement());


/* Er worden storage elementen aangemaakt */
function CreateStorageElement() {

    if(sessionStorage.getItem('ShoppingCart') == null) {
        let winkelMand = [] ;
        sessionStorage.setItem('ShoppingCart',JSON.stringify(winkelMand));
        // console.log(sessionStorage.getItem('ShoppingCart'));

    }

    if (sessionStorage.getItem('ItemBasket') == null){
        let counter = 0;
        sessionStorage.setItem('ItemBasket',counter)
    }
    if (sessionStorage.getItem('userId') != null){
        dropDownMenu();
    }
    console.log(window.location);

}





counteritem();

//Basket items, aantal items in winkelmand word weergegevn
function counteritem() {
    let counter = sessionStorage.getItem('ItemBasket');
    document.querySelector('#winkelmandje #aantal').innerHTML = counter;

}



// search item, maakt variabele van gezochte letters, en dan op de pagina zoeken naar de gezochten letters
// als die niet gevonden wordt dan word je naar de product pagina verwezen
function search() {
    const itemName = document.getElementById('searchBar').value;
    sessionStorage.setItem('searchItem',itemName);

    let page = "http://localhost:63342/project-werkplekleren/src/ProjectSS/html/producten.html";
    if (window.location.href != page){
        window.location.href = 'producten.html';
    }
    else {
        SearchData();
    }
}





// Get the modal (inlog venster)

var modal = document.querySelector('.modal');

document.addEventListener('click', function (e) {
    e = e || window.event;
    let target = e.target || e.srcElement,
        text = target.textContent || target.innerText;

    if (e.target == document.querySelector(".modal")) {
        closeLogin();
    }
}, false);


// om naar winkelmand te gaan, alleen als je ingelogd bent. als je niet ingelogd bent krijg je een melding
function openWinkelMand() {

    if (sessionStorage.getItem('userId') != null) {
        window.location.href = "winkelMand.html";
    }
    else {
            document.querySelector(".modal").style.display = "block";
            alert("You need have an account.");
    }

}


function closeLogin() {
    document.querySelector(".modal").style.display = "none";
}
function openLogin() {
    //  LoginScreen();
    document.querySelector(".modal").style.display = "block";
}


function openRegister() {
    window.location.href = "register.html";
}


// controllen voor mail.

function CheckEmail() {

    const email = document.getElementById("userName").value;

    fetch('https://localhost:44372/api/User/CheckMail?email='+email ,{
        method: 'GET'
    })
        .then(response => {
            return response.json();
        })
        .then((data) => {
            console.log(data);
            let checker = Boolean(data.Data);
            if (checker){
                Checkpsw();
            }
            else {
                alert("You're Email doesn't exist!!!")
            }

        })
}

// controllen voor psw
function Checkpsw() {

    const psw = document.getElementById('psw').value;

    fetch('https://localhost:44372/api/User/CheckPsw?psw='+psw,{
        method:'GET',
        headers:{
            'Content-Type':'application/json'
        }
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            let checker = Boolean(data.Data);

            if (checker){
                LoginUser();
            }
            else {
                alert("You're Password is wrong!!!")
            }
        })

}



//je krijgt een ID nummer van gebruiker terug als het correct is.
function LoginUser() {
    const email = document.getElementById("userName").value;
    const psw = document.getElementById('psw').value;

    fetch('https://localhost:44372/api/User?email='+email+'&psw='+psw ,{
        method:'get',
        headers: {
            'Content-Type':'application/json',
        }
    })
        /* haalt de data op en zet in een json file */
        .then((response) => response.json())
        /* hier wordt gekeken in de data object of er een userid bestaat die overeen komt met de gegevens
        * zo ja, wordt er een sessionStorage gecreerd en wordt daar de inlogsessie opgeslagen
        * als je localstorage doet dan moet je die ook verwijderen */
        .then((data) => {
            if (data.Data > -1){
                sessionStorage.setItem('userId',data.Data);
                UserName();
                dropDownMenu();
            }
        })
}

// krijg je de dropdownmenu te zien met de opties als gebruiker
function dropDownMenu(){
    document.querySelector(".modal").remove();
    document.querySelector("#login a").removeAttribute("onclick");

    let menu = document.querySelector('.dropdown-content');
    let icon = document.querySelector(".dropdown a");
    let open = (e)=>{
        menu.style.display='block';
    };
    let close = (e)=>{
        menu.style.display='none';
    };

    icon.addEventListener('mouseenter', open);
    menu.addEventListener('mouseenter', open);
    icon.addEventListener('mouseleave', close);
    menu.addEventListener('mouseleave', close);

    let userName = sessionStorage.getItem('userName');
    document.querySelector(".dropdown li a").innerHTML = userName;

    admin();
}


//Get User name, haalt gebruikersnaam op om in de dropdown menu weer te geven
function UserName() {

    let id = sessionStorage.getItem('userId');

    fetch('https://localhost:44372/api/Person/Name/'+id,{
        method:'GET'
    })
        .then(respone => {
            return respone.json();
        })
        .then(data => {

            console.log(data.Data);
            let userName = data.Data;

            sessionStorage.setItem("userName",userName);
        });

}


function logout() {

    sessionStorage.removeItem('userId');
    sessionStorage.removeItem('ShoppingCart');
    sessionStorage.removeItem('ItemBasket');
    sessionStorage.removeItem('ProductId');
    sessionStorage.removeItem('userName');
    sessionStorage.removeItem('userEmail');

    window.location.href = 'home.html';

    sessionStorage.removeItem('')

}


// admin login
function admin() {

    let id = sessionStorage.getItem('userId');

    if (id==1){
        let liElement1 = document.createElement("li");
        let aElement1 = document.createElement('a');
        aElement1.innerHTML = "Send Email";
        aElement1.setAttribute('href','sendMail.html');

        let liElement2 = document.createElement("li");
        let aElement2 = document.createElement('a');
        aElement2.innerHTML = "Edit Products";
        aElement2.setAttribute('href','EditProductData.html');

        let liElement3 = document.createElement("li");
        let aElement3 = document.createElement('a');
        aElement3.innerHTML = "Users";
        aElement3.setAttribute('href','Users.html');

        liElement1.appendChild(aElement1);
        liElement2.appendChild(aElement2);
        liElement3.appendChild(aElement3);


        let classElem = document.querySelector(".dropdown-content");
        classElem.appendChild(liElement1);
        classElem.appendChild(liElement2);
        classElem.appendChild(liElement3);

    }
}

// als je op bestsellers wordt u naar productdetails doorgestuurd
function openDetailPage(id) {

    sessionStorage.setItem('ProductId', id);
    window.location.href='productDetails.html';

}



