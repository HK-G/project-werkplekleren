
window.addEventListener('onload', GetUserData());

// bij het laden van de pagina wordt alle gebruikers info opgehaald
function GetUserData() {

    fetch('https://localhost:44372/api/Account/All')
        .then( (res) => res.json())
        .then( data => {
            console.log(data.Data);
            data.Data.forEach( (e) => {
                insertData(e);

            })

           // insertData(data.Data);
        })

}

// steekt dat in een tabel
function insertData(userData){

    let trElement = document.createElement('tr');
    trElement.setAttribute('id', userData.userid);

    console.log(userData.userid);

    output = `
            <td>${userData.userid}</td>
            <td>${userData.personid}</td>
            <td>${userData.firstname}</td>
            <td>${userData.name}</td>
            <td>${userData.email}</td>
            <td>${userData.password}</td>
            <td>${userData.birthdate.slice(0,10)}</td>
            <td>${userData.addressid}</td>
            <td>${userData.street}</td>
            <td>${userData.streetnumber}</td>
            <td>${userData.postalcode}</td>
            <td>${userData.city}</td>
            <td><input class="none" onclick="RemoveUser(${userData.userid})"  type="button" value="X" /></td>

     `;
    trElement.innerHTML = output;

    let tbody = document.getElementById('tableBody');
    tbody.appendChild(trElement);
}

// als je een gebruiker verwijdert, maakt de tabel leeg en met GetUserData wordt die opgevuld zie RemoveUser
function clearScreen() {

    let tableElement = document.querySelector('#tableBody');

    let user = tableElement.firstElementChild;

    while(user){
        tableElement.removeChild(user);
        user = tableElement.firstElementChild;
    }

}

// hiermee verwijder je een gebruiker,
function RemoveUser(id) {

    fetch('https://localhost:44372/api/Account/RemoveUser/'+id,{
        method:'Post',
        headers:{
            'Content-Type': 'application/json'
        }
    })
        .then( (res) =>{
            if (res.ok){
                clearScreen();
                GetUserData();
            }
        })


}