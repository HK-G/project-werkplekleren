
const id = sessionStorage.getItem('userId'); //window.localStorage.getItem('userId');


//document.getElementById('birthdate').value = '1996-12-22';
window.addEventListener('onload', GetUserDataInfo());

// gebruiker info inladen
function GetUserDataInfo() {

//    let id = sessionStorage.getItem('userId'); //window.localStorage.getItem('userId');

   fetch('https://localhost:44372/api/Account/'+id,{
        method:'get'
    })
        .then((response) => response.json())
        .then( (res) => {
            console.log(res.Data);
            console.log(res);

            res.Data.forEach((e) => {
                document.getElementById('voornaam').value = e.firstname;
                document.getElementById('achternaam').value = e.name;
                document.getElementById('birthdate').value = (e.birthdate.slice(0,10));
                document.getElementById('email').value = e.email;
                document.getElementById('password').value = e.password;
            })
        }).catch((error) => console.log(error));

}

// toestaan om te info te wijzigen
function EnabeleData() {

    let inputElement = document.querySelectorAll('form input');

    inputElement.forEach((e)=>{
        e.disabled = false;
    })
}

// wijzigingen opslaan en doorsturen naar de databank
function modifyData() {


    const fName = document.getElementById("voornaam").value;
    const lName = document.getElementById("achternaam").value;
    const geboorteDag = document.getElementById("birthdate").value;
    const email =document.getElementById('email').value;
    const pwd = document.getElementById("password").value;

    const data = {
        firstName: fName,
        lastName: lName,
        birthDate: geboorteDag,
        email: email,
        password: pwd
    };

    fetch('https://localhost:44372/api/Account/UpdateUser/'+id, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    })
        .then(response => {
            console.log(response);
            postEmail();
            if (response.ok){
                UserName();
                window.location.href = "home.html";
            }})


        .catch((error) => console.log(error));
}

// sluit de venster
function disabeleData() {

    let inputElement = document.querySelectorAll('form input');

    inputElement.forEach((e)=>{
        e.disabled = true;
    })
}


// stuur email met de nieuwe gegevens
function postEmail() {

    const Vnaam = document.getElementById("voornaam").value;
    const Anaam = document.getElementById("achternaam").value;
    const Birthdate = document.getElementById("birthdate").value;
    const email = document.getElementById("email").value;
    const psw = document.getElementById("password").value;


    const mailto = email;
    const mailsubject = "ProfielGegevens";
    const mailbody = `Uw profielgegevens zijn aangepast naar:\nVoornaam : ${Vnaam}\nAchternaam : ${Anaam}\nBirthdate : ${Birthdate}
    \nemail : ${email}\nPasswoord : ${psw}`;
    const data = {
        mailto: mailto,
        mailsubject: mailsubject,
        mailbody: mailbody,
    };
    fetch('https://localhost:44372/api/Mail/',  {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    })
        .then( (result) => {
            console.log(result);
            console.log(result.data);
        });
}