


// voegt nieuwe product toe en stuurt de gegevens naar de databank
function CreateProduct() {

    const title = document.getElementById('title').value;
    const genre = document.getElementById('genre').value;
    const description = document.getElementById('description').value;
    const price =  parseFloat(document.getElementById("price").value).toFixed(2); // document.getElementById('price').value;
    const language = document.getElementById('taal').value;
    const author = document.getElementById('author').value;
    const pages = document.getElementById('pages').value;

    const data = {
        title: title,
        price: price,
        author: author,
        language: language,
        category: genre,
        description: description,
        pages:pages,
    };

    fetch('https://localhost:44372/api/Product/',{
        method:"POST",
        headers: {
            'Content-Type':'application/json',
        },
        body:JSON.stringify(data),
    })
        .then(response =>  {
            console.log(response);
            if (response.ok){
                document.getElementById('title').value = "";
                document.getElementById('genre').value = "";
                document.getElementById('description').value = "";
                document.getElementById("price").value = 0;
                document.getElementById('taal').value = "";
                document.getElementById('author').value = "";
                document.getElementById('pages').value =  0;
            }
        })
}

