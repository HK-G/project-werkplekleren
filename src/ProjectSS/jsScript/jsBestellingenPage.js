window.addEventListener('onload',GetOrder());

// haalt gegevens van eerdere bestellingen op
function GetOrder(){

    const id = sessionStorage.getItem('userId');

    fetch('https://localhost:44372/api/Order/'+id)
        .then(res => res.json())
        .then(data => {
           console.log(data.Data);
            data.Data.forEach( (product) => {
                OrderTable(product);
            });
        })
}


// vult div in met de gegevens van de eerdere bestellingen
function OrderTable(order){

    let output = `
            <div class="datumbesteld">
                <label class="dateOrdered">${order.date}</label>
            </div>
            <div class="imgimg"><img class="bookImage" src="../html/bookImage/${order.productname}.jpg" alt="bookName"></div>
            <div class="titletitle"><h2>${order.productname}</h2></div>
            <div  class="amountamount">
                <label class="aantalkeerbesteld">${order.amount}</label>
            </div>
            <div  class="priceprice"><p class="price">€${order.price}</p></div>
            <div class="knoppen">
                <div class="addcart">
                    <img id="addToBasket" onclick="AddBasket(${order.productid})" src="../assets/shopping-cart.svg" alt="Voeg toe aan winkelmandje">
                </div>
            </div>
    `;

    let orderbox = document.createElement('div');
    orderbox.className = "container11";
    orderbox.innerHTML = output;

    let element = document.querySelector('.containerbestelling');
    element.appendChild(orderbox);

}


// hir kan je oude bestellingen weer toevoegen aan jouw winkelmandje
function AddBasket(id) {
    let objShoppingCart = JSON.parse(sessionStorage.getItem('ShoppingCart'));

    //objShoppingCart = objShoppingCart.sort();

    if (!objShoppingCart.includes(id)){ // check if its in the basket.
        objShoppingCart.push(id);
        sessionStorage.setItem('ShoppingCart',JSON.stringify( objShoppingCart.sort()));

        let counter = Number(sessionStorage.getItem('ItemBasket'));
        counter = Number(counter) + 1;
        document.querySelector('#winkelmandje #aantal').innerHTML = counter;
        sessionStorage.setItem('ItemBasket',counter)
    }
    else {
        alert('This Item is already in the Shopping Basket');
    }

}
