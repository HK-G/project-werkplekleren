
// als u een vraag hebt kunt met deze functie een mail sturen
function postEmail() {

    const lastname =  document.getElementById('name').value;
    const firstname = document.getElementById('fname').value;
    const emailUser = document.getElementById('email').value;
    const userQuest = document.getElementById('vraag').value;

    let subject = "quest from "+lastname+" "+firstname ;

    let text = `is een vraag van ${lastname}  ${firstname}
        \nemail is  ${emailUser}
        \nvraag:\n  ${userQuest};
        `;

    const mailto = "schoolboekenspullen@gmail.com";
    const mailsubject = subject;
    const mailbody = text;
    const data = {
        mailto: mailto,
        mailsubject: mailsubject,
        mailbody: mailbody,
    };
    fetch('https://localhost:44372/api/Mail/',  {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    })
        .then( (result) => {
            if (result.ok){
                document.getElementById('name').value ="";
                document.getElementById('fname').value ="";
                document.getElementById('email').value ="";
                document.getElementById('vraag').value ="";
            }
        })
}