window.addEventListener('onload', GetProductNaam());

// haalt productnaam en ID op van de datatable
function GetProductNaam() {

    fetch("https://localhost:44372/api/Product/Name")
        .then(response => response.json())
        .then(data => {
            data.Data.forEach((names) => {
               // console.log(names.Productid,names.ProductName);
                ProductNames(names.Productid ,names.ProductName);
            });

            document.getElementById("TitleBook").value = "-1";

        })
}

// geeft alle productnamen weer, wat die heeft opgehaald in een dropdownmenu weer
function ProductNames(id,name) {

    optionElement = document.createElement("option");
    optionElement.value = id;
    optionElement.innerHTML = name;

    document.getElementById("TitleBook").appendChild(optionElement);
}

// haalt gegevens op van de geselecteerde product van de dropdown menu
function getProduct() {

    let productId = document.getElementById('TitleBook').value;


    fetch('https://localhost:44372/api/Product/'+productId )
        .then((res) => res.json())
        .then((product) => {
            //console.log(product.Data);
            document.getElementById('title').value = product.Data.ProductName;
            document.getElementById('genre').value = product.Data.Category;
            document.getElementById('description').value = product.Data.Description;
            document.getElementById("price").value = product.Data.Price;
            document.getElementById('taal').value = product.Data.Language;
            document.getElementById('author').value = product.Data.Author;
            document.getElementById('pages').value =  product.Data.Pages;
        })
}

/*
function productDetails(id,title,author, language,price,description,catagorie, aantalPaginas ) {
    document.getElementById('title').value = product.Data.ProductName;
    document.getElementById('genre').value = product.Data.Category;
    document.getElementById('description').value = product.Data.Description;
    document.getElementById("price").value = product.Data.Price;
    document.getElementById('taal').value = product.Data.Language;
    document.getElementById('author').value = product.Data.Author;
    document.getElementById('pages').value =  product.Data.Pages;
}*/

// stuurt de wijziginen van een product door naar datatable
function updateProduct() {
    const  id = document.getElementById("TitleBook").value;
    const title = document.getElementById('title').value;
    const category = document.getElementById('genre').value;
    const description = document.getElementById('description').value;
    const price = document.getElementById("price").value.replace(',','.');
    const taal = document.getElementById('taal').value;
    const author = document.getElementById('author').value;
    const pages = document.getElementById('pages').value;



    if (id != ""){
        
        const data ={
            id:id,
            title: title,
            price: price,
            author: author,
            language: taal,
            category: category,
            description: description,
            pages:pages,
        };

        fetch('https://localhost:44372/api/Product/Update/'+id, {
            method:'POST',
            headers: {
                'Content-Type':'application/json',
            },
            body:JSON.stringify(data),
        })
            .then((response) => {
                if (response.ok){
                    console.log(data);
                    document.getElementById('title').value = "";
                    document.getElementById('genre').value = "";
                    document.getElementById('description').value = "";
                    document.getElementById("price").value = 0;
                    document.getElementById('taal').value = "";
                    document.getElementById('author').value = "";
                    document.getElementById('pages').value =  0;

                    removeOption();
                    GetProductNaam();
                }
            })
            .catch( console.error())
    }
    else {
        alert("You need to select a product name.")
    }

}

/*function removeOption() {

    let selectElement = document.getElementById('TitleBook');
    var i  = selectElement.options.length - 1;
    for (i; i >= 0; i--) {
        selectElement.remove(i);
    }

}*/
