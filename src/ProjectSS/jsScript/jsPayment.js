// checkboxen worden unchecked gelaad
window.onload = function uncheck() {
    document.getElementById("afhaalpunt").checked = false;
    document.getElementById("leverthuis").checked = false;
};

// verbergt afleveradres, creditcart, maestro en paypal
window.onload = function hideAfleverAdres(){
    document.getElementById("afleverAdres").style.display = 'none';
    document.querySelector(".gegevensCreditcart").style.display = 'none';
    document.querySelector(".gegevensMaestro").style.display = 'none';
    document.querySelector(".gegevensPaypal").style.display = 'none';

    // totaalprijs van winkelmand opgehaald
    let totaleprijs = sessionStorage.getItem('stuurTotalePrijs');
    document.getElementById('tebetalenbedrag').innerText = totaleprijs;

    GetAddress();

}



/* LEVERING PLAATS */

function checker() {

    if (document.getElementById("afhaalpunt").checked == true)
    {
        document.getElementById("leverthuis").checked = false;
        document.getElementById("afleverAdres").style.display = 'none';
    }
}
function checkerT() {
    if (document.getElementById("leverthuis").checked == true) {
        document.getElementById("afhaalpunt").checked = false;
        document.getElementById("afleverAdres").style.display = 'inline';
    }
}

/* HOE LEVEREN */

function checkernormalelevering() {
    if (document.getElementById("normaallevering").checked == true)
    {
        document.getElementById("expreslevering").checked = false;
    }
}

function checkerexpreslevering() {
    if (document.getElementById("expreslevering").checked == true)
    {
        document.getElementById("normaallevering").checked = false;
    }
}

/* BETAALWIJZE */

function checkerpaypal() {
    if (document.getElementById("paypal").checked == true)
    {
        document.getElementById("creditcart").checked = false;
        document.getElementById("maestro").checked = false;
        document.querySelector(".gegevensPaypal").style.display = 'inline';
        document.querySelector(".gegevensCreditcart").style.display = 'none';
        document.querySelector(".gegevensMaestro").style.display = 'none';
    }
}

function checkercreditcart() {
    if (document.getElementById("creditcart").checked == true)
    {
        document.getElementById("paypal").checked = false;
        document.getElementById("maestro").checked = false;
        document.querySelector(".gegevensCreditcart").style.display = 'inline';
        document.querySelector(".gegevensMaestro").style.display = 'none';
        document.querySelector(".gegevensPaypal").style.display = 'none';
        GetCreditcartData();
    }
}


function GetCreditcartData() {
    const id = sessionStorage.getItem('userId');

    fetch('https://localhost:44372/api/Card/KredietCard/'+id)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if (data.Data != "No CardData Found"){
                document.getElementById("naamCreditcart").value = data.Data.Naam;
                document.getElementById("nummerCreditCart").value = data.Data.KredietKaartNummer;
                document.getElementById("geldigMaandC").value = data.Data.GeldigheidMaand;
                document.getElementById("geldigJaarC").value = data.Data.GeldigheidJaar;
                document.getElementById("cvc").value = data.Data.Cvc;
                document.getElementById("bewaarCreditcart").checked = true;
            }
        })
}

function SaveCreditCartData() {

    const id = sessionStorage.getItem('userId');
    const name = document.getElementById('naamCreditcart').value;
    const kaartnummer = document.getElementById('nummerCreditCart').value;
    const geldigmaand = document.getElementById('geldigMaandC').value;
    const geldigjaar = document.getElementById('geldigJaarC').value;
    const Cvc = document.getElementById("cvc").value;

    const data = {
        userid:id,
        Name:name,
        kaartNummer:kaartnummer,
        geldigJaar:geldigjaar,
        geldigMaand:geldigmaand,
        cvc:Cvc
    };



    const bewaar = document.getElementById('bewaarCreditcart').checked;

    if (bewaar == true){
        fetch('https://localhost:44372/api/Card/KredietCard',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        })
            .then(res => {
                if(res.ok){
                    console.log("ok");
                }
            })
    }
    else {

        sessionStorage.setItem('credietCardData',JSON.stringify(data))
    }


}




function checkermaestro() {
    if (document.getElementById("maestro").checked == true)
    {
        document.getElementById("creditcart").checked = false;
        document.getElementById("paypal").checked = false;
        document.querySelector(".gegevensMaestro").style.display = 'inline';
        document.querySelector(".gegevensCreditcart").style.display = 'none';
        document.querySelector(".gegevensPaypal").style.display = 'none';
        GetMaestrocartData();
    }
}

function GetMaestrocartData() {
    const id = sessionStorage.getItem('userId');

    fetch('https://localhost:44372/api/Card/MaestroCard/'+id)
        .then(res => res.json())

        .then(data => {
            console.log(data);
            if (data.Data != "No CardData Found"){
                document.getElementById("naamMaestro").value = data.Data.Naam;
                document.getElementById("kaartnrMaestro").value = data.Data.Maestronummer;
                document.getElementById("geldigMaandM").value = data.Data.GeldigheidMaand;
                document.getElementById("geldigJaarM").value = data.Data.GeldigheidJaar;
                document.getElementById("bewaarMaestro").checked = true;
            }


        })
}

function SaveMaestroCartData() {

    const id = sessionStorage.getItem('userId');
    const name = document.getElementById('naamMaestro').value;
    const kaartnummer = document.getElementById('kaartnrMaestro').value;
    const geldigmaand = document.getElementById('geldigMaandM').value;
    const geldigjaar = document.getElementById('geldigJaarM').value;

    const data = {
        userid:id,
        Name:name,
        kaartNummer:kaartnummer,
        geldigJaar:geldigjaar,
        geldigMaand:geldigmaand,
    };



    const bewaar = document.getElementById('bewaarMaestro').checked;

    if (bewaar == true){
        fetch('https://localhost:44372/api/Card/MaestroCard',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        })
            .then(res => {
                if(res.ok){
                    console.log("ok");
                }
            })
    }
    else {

        sessionStorage.setItem('MaestroCardData',JSON.stringify(data))
    }


}

/* ALLEEN NUMMER OF SLASH FORWARD KUNNEN INGEVEN */
/*
function keypadRestricts(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ( charCode > 31 && (charCode < 47 || charCode > 57))
        return document.getElementById("errmsg").innerHTML = "Geef alleen nummer of / in!";

    return true;
    /!*return document.getElementById("errmsg").style.display = 'none'*!/
}*/

/* als u op de knop bestellen drukt */

function clickConfirm() {

    if (document.getElementById("paypal").checked == true)
    {
     window.location.href = 'https://www.paypal.com/';
    }
    if(document.getElementById("creditcart").checked == true){
        SaveCreditCartData();
    }
    if (document.getElementById("maestro").checked == true) {
        SaveMaestroCartData();
    }
    PlaceOrder();

}

function clickCancel() {

    let winkelMand = [];
    sessionStorage.setItem('ShoppingCart',JSON.stringify(winkelMand));

    window.location.href = "Producten.html";
}


/* GetAddress */
// adres gegevens worden opgehaald
function GetAddress() {

    const id = sessionStorage.getItem('userId');

    fetch('https://localhost:44372/api/Person/Address/'+id)
        .then((res) => res.json())
        .then((data) => {
            console.log(data);
            if (data.Data != 'no Address Data Found'){
                document.getElementById('afleverAdresStraat').value = data.Data.Street;
                document.getElementById('afleverAdresStraatNummer').value = data.Data.StreetNumber;
                document.getElementById('afleverAdresStad').value = data.Data.City;
                document.getElementById('afleverAdresPostcode').value = data.Data.PostalCode;
            }

        })

}
// adres gegevens aanmaken
function CreateAddress() {


    const id = sessionStorage.getItem('userId');
    const straat = document.getElementById("afleverAdresStraat").value;
    const straatnr = document.getElementById('afleverAdresStraatNummer').value;
    const gemeent = document.getElementById('afleverAdresStad').value;
    const postcode = document.getElementById('afleverAdresPostcode').value;

    const data = {
        userid:id,
        street: straat,
        streetnumber:straatnr,
        postalcode: postcode,
        city:gemeent,
    }

    fetch("https://localhost:44372/api/Person/Address/",{
        method:'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    })
        .then(res => {
            if (res.ok){
                console.log('ok');
            }
        })
}

// geeft toestemming om adres te wijzigen
function EnabeleData() {

    let inputElement = document.querySelectorAll('#afleverAdres form input');
    let enable = false;

    inputElement.forEach((e)=>{
        if (e.disabled){
            e.disabled = false;
        }
        else{
            e.disabled = true;
        }
    })

    const straat = document.getElementById("afleverAdresStraat").value;
    if ((inputElement[0].disabled == true) && straat != "" ){
        CreateAddress();
    }
}





//////check this
function PlaceOrder() {

    GetMail();

}

function GetMail() {
    const id = sessionStorage.getItem('userId');

    fetch('https://localhost:44372/api/Account/'+id)
        .then(res => res.json())
        .then( data => {
            console.log(data.Data[0].email);
            sessionStorage.setItem('userEmail', data.Data[0].email);

           postEmail();
        })

}

function postEmail(){

    let text = 'Uw bestelling is onderweg!';
    let email = sessionStorage.getItem('userEmail');
    const mailto = `${email}`;
    const mailsubject = "Bestelling";
    const mailbody = text;
    const data = {
        mailto: mailto,
        mailsubject: mailsubject,
        mailbody: mailbody,
    };
    fetch('https://localhost:44372/api/Mail/',  {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    })
        .then( (result) => {

            sessionStorage.removeItem('ItemBasket');
            sessionStorage.removeItem('ShoppingCart');

            if (result.ok){
                window.location.href ="home.html";
            }
        })
    //.then(logResult)
    //.catch(logError);
}
