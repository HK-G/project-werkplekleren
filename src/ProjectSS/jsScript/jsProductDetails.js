
window.addEventListener('onload',getProduct());

let productId = sessionStorage.getItem("ProductId");
//alert(productId);

// haalt productdata op
function getProduct() {

    let productId = sessionStorage.getItem("ProductId");


    fetch('https://localhost:44372/api/Product/'+productId )
        .then((res) => res.json())
        .then((product) => {
            console.log(product.Data);
            ProductenElements(product.Data.Productid, product.Data.ProductName,product.Data.Author,product.Data.Language,
                        product.Data.Price,product.Data.Description,product.Data.Category,product.Data.Pages);
        })
}

// geeft productdata weer in elementen
function ProductenElements(id,title,author, language,price,description,catagorie, aantalPaginas ) {

    console.log(id);


    let  image = `bookImage/${title}.jpg`;

    let imageBook = document.createElement("img");
    imageBook.src = image;
    imageBook.alt = title;


    document.querySelector('.imgBook').appendChild(imageBook) ;

    document.getElementById('titelBoek').innerText = title;
    document.getElementById('schrijver').innerText = author;
    document.getElementById('taal').innerText = language;
    document.getElementById('prijsBoek').innerText = "€"+price;
    document.querySelector('.omschrijving p').innerText = description;

    let Specimage = document.createElement("img");
    Specimage.src = image;
    Specimage.alt = title;

    document.querySelector('.specfoto').appendChild(Specimage);
    document.getElementById('uitgever').innerHTML += author;
    document.getElementById('page').innerHTML += aantalPaginas;
    document.getElementById('catagorie').innerHTML += catagorie;

    let btn = document.querySelector('.BtnAddWinkelmand');
    btn.setAttribute('onclick',`AddBasket(${id})`);


}

// product id's in the basket.
function AddBasket(id) {

    if (sessionStorage.getItem('userId') != null){
        let objShoppingCart = JSON.parse(sessionStorage.getItem('ShoppingCart'));

        //objShoppingCart = objShoppingCart.sort();


        if (!objShoppingCart.includes(id)){ // check if its in the basket.
            objShoppingCart.push(id);
            sessionStorage.setItem('ShoppingCart',JSON.stringify( objShoppingCart.sort()));

            let counter = Number(sessionStorage.getItem('ItemBasket'));
            counter = Number(counter) + 1;
            document.querySelector('#winkelmandje #aantal').innerHTML = counter;
            sessionStorage.setItem('ItemBasket',counter);
        }
        else {
            alert('This Item is already in the Shopping Basket');
        }
    }else {
        document.querySelector(".modal").style.display = "block";
        alert("You need have an account.");
    }

}
