window.addEventListener("load", check => {

    if (sessionStorage.getItem('searchItem') != null){
        SearchData();
    }
    else {
        WindowCard();
    }



});


// producten opzoeken ahv naam
function SearchData(){

    let nameProduct =  sessionStorage.getItem('searchItem');

    fetch('https://localhost:44372/api/Product/Search/'+nameProduct, {
        method: 'GET',
    })
        .then( res => res.json())
        .then( data => {
            console.log(data);
            clearWindowProduct();
            data.Data.forEach(function (product) {
                //console.log(product);
                windowbooks(product.Productid, product.ProductName, product.Price, product.Category);

            });
        })

    sessionStorage.removeItem('searchItem')
}


/// get product id went click on the product.

function openDetailPage(id) {

    sessionStorage.setItem('ProductId', id);
    window.location.href='productDetails.html';

}
////
valueSelecterPrijs();

function valueSelecterPrijs() {
    let maxPrijs = document.getElementById('maxPrijs');
    let minPrijs = document.getElementById('minPrijs');
 // 14/4/2020  / 06:44
    //tijdelijke
    for (let i = 10; i <= 500; i+=10){
        let moneyPoints = document.createElement("option");
        moneyPoints.value = i;
        moneyPoints.innerHTML = i;

        minPrijs.appendChild(moneyPoints);

    }
    for (let i = 10; i <= 500; i+=10){
        let moneyPoints = document.createElement("option");
        moneyPoints.value = i;
        moneyPoints.innerHTML = i;
        maxPrijs.appendChild(moneyPoints);

    }
    minPrijs.value = 0;
    maxPrijs.value = 9999;
}


// alle producten ophalen en stuurt door naar windowbooks
function WindowCard() {

    fetch('https://localhost:44372/api/product')
        .then((res) => res.json())
        .then((data) => {
           // console.log(data.Data);
            data.Data.forEach(function (product) {
                //console.log(product);
                windowbooks(product.Productid, product.ProductName, product.Price, product.Category);
            });

        })
}

// maakt elementen aan met de gegevens van de producten erin(parameters)
function windowbooks( id ,title, price,category) {

    /*Face One */

    let window = document.createElement('div');
    window.className = "books";


    /*face Two*/

    let  output = `
            <img class="bookImage" onclick="openDetailPage(${id})" src="bookImage/${title}.jpg" alt="achtergrond">
            <div class="bookInfo">
                <p class="titel">
                    ${title}
                </p>
                <div class="bookInfoBottom">
                <p class="prijs">Prijs:</p>
                 <b> ${price}</b> 
                 &nbsp;&nbsp
                <p class="prijs">Category:</p>
                  <b> ${category}</b>
                <img class="basketImage" id="${id}"  onclick='AddBasket(${id})' src="../assets/shopping-cart.svg" alt="Flowers in Chania">
                </div>
            </div>
    `;

    window.innerHTML = output;

    let getELement = document.querySelector('.product-Box');
    getELement.appendChild(window);

}



////////////////////////////////////////////////////////////////////////////////
window.addEventListener("load", (e) => {

    if(sessionStorage.getItem('ShoppingCart') == null) {
        let winkelMand = [];
        sessionStorage.setItem('ShoppingCart',JSON.stringify(winkelMand));
       // console.log(sessionStorage.getItem('ShoppingCart'));
    }

});


// product id's in the basket.
function AddBasket(id) {

    if (sessionStorage.getItem('userId') != null) {

        let objShoppingCart = JSON.parse(sessionStorage.getItem('ShoppingCart'));

        objShoppingCart = objShoppingCart.sort();

        if (!objShoppingCart.includes(id)) { // check if its in the basket.
            objShoppingCart.push(id);
            sessionStorage.setItem('ShoppingCart', JSON.stringify(objShoppingCart.sort()));

            let counter = Number(sessionStorage.getItem('ItemBasket'));
            counter = Number(counter) + 1;
            document.querySelector('#winkelmandje #aantal').innerHTML = counter;
            sessionStorage.setItem('ItemBasket', counter)

        } else {
            alert('This Item is already in the Shopping Basket');
        }

    } else {
        document.querySelector(".modal").style.display = "block";
        alert("You need have an account.");
    }
}


///////

//// filtert
// maakt window leeg voordat de producten opnieuw opgehaald worden
function clearWindowProduct() {

    let getELement = document.querySelector('.product-Box');
    let books = getELement.firstElementChild;
    while (books){
        getELement.removeChild(books);
        books = getELement.firstElementChild;
    }

}

// hier worden de producten ophehaald
function filterProduct() {

    const categorie = document.getElementById('categorie').value;
    const minpPrijs = document.getElementById('minPrijs').value;
    const maxPrijs = document.getElementById('maxPrijs').value;
    const taal = document.getElementById('taal').value;

    fetch(`https://localhost:44372/api/Product/Filter/${categorie}/${minpPrijs}/${maxPrijs}/${taal}`)
    .then((res) => res.json())
    .then((data) => {
        clearWindowProduct();
        data.Data.forEach(function (product) {
            windowbooks(product.Productid, product.ProductName, product.Price, product.Category);

        });
    })

}





