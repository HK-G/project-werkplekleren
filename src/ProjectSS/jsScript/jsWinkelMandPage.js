
let items = JSON.parse(sessionStorage.getItem('ShoppingCart'));


window.addEventListener('load', getProduct());



// haalt alle gegevens van de gekozen producten uit de databank
function getProduct() {

    fetch('https://localhost:44372/api/Product/Winkelmand/'+items, {
        method:'get'
    })
        .then((res) => res.json())
        .then((data) => {
            console.log(data);
            data.Data.forEach((product) => {
            AddProduct(product.Productid, product.ProductName, product.Price);
        })
        })
};


// vult de winkelmand met elementen van de gegevens van producten in
function AddProduct(id,bookTitle,price) {

    let image = `bookImage/${bookTitle}.jpg`;

    //verwijderd space voor id attribut
    let amount =  bookTitle.split(" ");  // bookTitle.match(/([\s]+)/g).length; > // kan soms fout melding geven bij types zonder spaties.
    let titleId = bookTitle;

    //console.log(amount);

    for (let i = 0; i < amount.length; i++) {
       titleId = titleId.replace(' ', '');
      // console.log(titleId);
    }

    titleId = titleId.toLowerCase();


    let itemBox = document.createElement("div");
    itemBox.className = 'item';
    //itemBox.setAttribute('id', titleId);

    let output = `
            <div id="${titleId}" data-name="${bookTitle}" data-idProduct="${id}" class="bodybody">
                <div class="imgimg"><img class="bookImage" src="${image}" alt="bookName"></div>
                <div class="titletitle"><h2>${bookTitle}</h2></div>
                <div  class="amountamount">
                    <div class="amount">
                    <button class="amountChanges"  onclick="changeAmountItem('${titleId}',-1,${price})" >-</button>
                    <span>1</span>
                    <button  class="amountChanges"  onclick="changeAmountItem('${titleId}',1,${price})" >+</button>
                    </div>
                </div>
                <div  class="priceprice"><p class="price">€${price}</p></div>
                <div  class="btnbtn">
                    <input class="none" onclick="deleteProduct('${titleId}',${id}) " type="button" value="X" />
                </div>
            </div>
           `;

    itemBox.innerHTML = output;

    let boekelement = document.querySelector('.bodywinkelmand');
        boekelement.appendChild(itemBox);
    totaalPrice();

}



function changeAmountItem(amountOFbook, num,price) {

    let amoutItem = document.querySelector(`#${amountOFbook} .amount span`).innerHTML;
    let priceProduct;

    if (amoutItem >= 1){
        amoutItem = Number(amoutItem) + Number(num);
        if (amoutItem <1){
            amoutItem = 1;
        }

        priceProduct =  price * amoutItem;

        document.querySelector(`#${amountOFbook} .amount span`).innerHTML = amoutItem;
        document.querySelector(`#${amountOFbook} .price`).innerHTML = '€'+priceProduct.toFixed(2);
    }
    totaalPrice();


}

function deleteProduct(idElement,idProduct) {

    let element = document.getElementById(idElement);
    element.remove(idElement);

    let position = items.indexOf(idProduct);

    if (position > 0 && position < items.length-1 ) {

        items = items.slice(0,position).concat(items.slice(position+1, items.length));
    }
    else if(position==0){
        items.shift();
        //alert("shift");
    }
    else if (position == items.length-1){
        items.pop();
    }
    console.log(items);
    let counter = Number(sessionStorage.getItem('ItemBasket'));
    counter = Number(counter) - 1;
    document.querySelector('#winkelmandje #aantal').innerHTML = counter;
    sessionStorage.setItem('ItemBasket',counter);

    totaalPrice();
    sessionStorage.setItem('ShoppingCart',JSON.stringify(items));
}


//
function totaalPrice() {

    let price = document.querySelectorAll('.price');
    let totaal =0;

    price.forEach((e)=>{
        totaal += Number( e.innerText.substring(1));
    });
    document.getElementById("totaalPrice").innerText = "€"+totaal.toFixed(2);


}

function CheckOut() {
    let bodymand = document.querySelector('.bodywinkelmand').firstElementChild;

    if(bodymand == null)
    {
        let waarschuwing = "Uw winkelmandje is leeg, gelieve producten toe te voegen om verder te gaan!";
        alert(waarschuwing);
    }
    else {
        OrderItems();
    }

    let totalPrice = document.getElementById("totaalPrice").innerText;
    sessionStorage.setItem('stuurTotalePrijs',totalPrice);

}

// bestelling wordt in de databank geplaatst
function OrderItems() {
    let orderItems = [];

    let Items = document.querySelectorAll('.item');
    let producten = "";
    let amount = "";


    for (let i=0;i < items.length; i++){
        let id = document.querySelectorAll('.item div[data-idproduct]')[i].dataset.idproduct;
        producten += id+',';
        amount += document.querySelectorAll('.item .amount span')[i].innerHTML+',';


    }

    producten = producten.substring(0, producten.length - 1);
    amount = amount.substring(0, amount.length - 1);


    const id =  sessionStorage.getItem("userId");


    const data = {
        userid:id,
        producten: producten,
        amount:amount
    };

    fetch('https://localhost:44372/api/Order',{
        method:"post",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(res => {
            if(res.ok){
                window.location.href = "Payment.html";
            }
        })

}


