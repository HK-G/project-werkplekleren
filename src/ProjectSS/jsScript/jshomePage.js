
/* slider commercials Home Page*/
let postion = -1;
function nextSlider(n) {
    postion += n;
    currentSlider(postion);
}


function currentSlider(n) {
    let imageP = document.querySelector(".slideShowImage");
    let number = n;
    number += '00';
    if (number > -100 && number < 500){
        imageP.style.right = number+'%';
    }
    else {
        postion = -1;
    }
}

autoChange();

function autoChange() {
    nextSlider(1);
    setTimeout(autoChange,5000);
}

/* ***********  */

/* best sellers */

testBestSellerInfo();

// bestsellers worden opgehaald
function testBestSellerInfo() {

    fetch('https://localhost:44372/api/Product')
        .then((res) => res.json())
        .then((data) => {

            for (let i =0; i < 6; i++){
               // console.log(data.Data);
                bestSellers(data.Data[i]);
            }
        })
    /*
    bestSellers('Unacknowledged','unack.jpg',38.66, 352);
    bestSellers('Nothing','nothing.jpg',44.99, 298);
    bestSellers('Rich Dad Poor Dad','rdpd.jpg',9.99, 352);
    bestSellers('Programmeren C#','pc.jpg',19.99, 199);
    bestSellers('The Hobbit','th.jpg',39.45, 458);
    bestSellers('The Lord of the Rings','tlofr.jpg',50, 1453);
    */
}

// creert elementen om info van de product in te steken
function bestSellers(product) {

    let bSellers = document.createElement("div");
    bSellers.className ="container";

    bSellers.setAttribute("onclick", `openDetailPage(${product.Productid})`);


    let output= `
       <h2>${product.ProductName}</h2>
       <div class="bookcover">
            <img src="bookImage/${product.ProductName}.jpg" alt="BestSellerBook ${product.ProductName}">
        </div>
        <div class="info">
            <div class"prijs">
                <b>€ ${product.Price}</b>
                <p>This book has ${product.Pages} Pages</p>
            </div
        </div>
       </div>
     `;

    bSellers.innerHTML = output;

    let getELement = document.querySelector(".boekensales");
    getELement.appendChild(bSellers);

}