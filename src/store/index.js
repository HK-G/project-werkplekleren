import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    CartCounter: 0,
    cart: [],
    books: [
      {
        name: 'Say Nothing',
        image: 'nothing.jpg',
        price: 35.99
      },
      {
        name: 'Programmeren C',
        image: 'pc.jpg',
        price: 45.99
      },
      {
        name: 'Rich Dad Poor Dad',
        image: 'rdpd.jpg',
        price: 9.99
      },
      {
        name: 'The Hobbit',
        image: 'th.jpg',
        price: 29.89
      },
      {
        name: 'The Lord of the Rings',
        image: 'tlofr.jpg',
        price: 150.00
      },
      {
        name: 'Unacknowledged',
        image: 'unack.jpg',
        price: 22.89
      }
    ]
  },
  mutations: {
    addProductToCart(state, payload) {
      let bookInCart = state.cart.find(cartBook => {
        return cartBook.name === payload.book.name;
      })

      if(bookInCart){
        bookInCart.amount +=1;
        bookInCart.total = bookInCart.amount * bookInCart.price;
      }else{
        payload.book.amount = 1;
        payload.book.total = payload.book.price;
        state.cart.push(payload.book);
      }

    },
    updateProductAmount(state,payload){
      let bookInCart = state.cart.find(cartBook => {
        return cartBook.name === payload.book.name;
      });
      if(bookInCart){
        bookInCart.amount = payload.newAmount;
        bookInCart.total = bookInCart.amount * bookInCart.price;
      }

    },
    deleteProductFromCart(state,payload){
      state.cart.splice(state.cart.indexOf(payload.book), 1);
    },
    deleteCart() {
      this.state.cart = [];
    }
  },
  actions: {
  },
  modules: {
  },
})
